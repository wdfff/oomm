#!/usr/bin/env bash
sh_ver="0.0.1"
zcash_ver="2.0.2"
readonly zcash_sha256sum="54171c6baf853d306525336d09dc1acab723573f64d1d82efa6b3a975a3354db"
user_home=$HOME

Green_font_prefix="\033[32m" && Red_font_prefix="\033[31m" && Green_background_prefix="\033[42;37m" && Red_background_prefix="\033[41;37m" && Font_color_suffix="\033[0m"
# Info="${Green_font_prefix}[信息]${Font_color_suffix}"
Info="${Green_font_prefix}[信息:] "
Error="${Red_font_prefix}[错误]${Font_color_suffix}"
Tip="${Green_font_prefix}[注意]${Font_color_suffix}"

# 检查root权限
check_root(){
	[[ $EUID != 0 ]] && echo -e "${Error} 当前账号非ROOT(或没有ROOT权限)，无法继续操作，请使用${Green_background_prefix} sudo su ${Font_color_suffix}来获取临时ROOT权限（执行后会提示输入当前账号的密码）。" && exit 1
}
# 检查系统
check_sys(){
	check_root
	if [[ -f /etc/redhat-release ]]; then
		release="centos"
	elif cat /etc/issue | grep -q -E -i "debian"; then
		release="debian"
	elif cat /etc/issue | grep -q -E -i "ubuntu"; then
		release="ubuntu"
	elif cat /etc/issue | grep -q -E -i "centos|red hat|redhat"; then
		release="centos"
	elif cat /proc/version | grep -q -E -i "debian"; then
		release="debian"
	elif cat /proc/version | grep -q -E -i "ubuntu"; then
		release="ubuntu"
	elif cat /proc/version | grep -q -E -i "centos|red hat|redhat"; then
		release="centos"
    fi
   [[ ${release} != "ubuntu" ]]  && [[ ${release} != "debian" ]]  && echo -e "${Error} 本脚本不支持当前系统 ${release} ! 目前只支持debian！ " && exit 1

}


# 安装依赖
Installation_dependency(){
	apt-get update 
	echo -e "${Info} apt-get update  完成 !"
	apt-get install g++ 
	echo -e "${Info} apt-get install g++  完成 !"
	apt-get install jq
	echo -e "${Info} apt-get install jq  完成 !"
}

# 下载zcash 
ZCASH_Dowmload(){
	cd "${user_home}"
	wget -N --no-check-certificate "https://z.cash/downloads/zcash-${zcash_ver}-linux64.tar.gz"
	# 下载完成检测	
	[[ ! -e "zcash-${zcash_ver}-linux64.tar.gz" ]] && echo -e "${Error}  zcash压缩包 下载失败 !" && rm -rf "zcash-${zcash_ver}-linux64.tar.gz" && exit 1
	# hash校验
	myhash=$(sha256sum "zcash-${zcash_ver}-linux64.tar.gz" | cut -d' ' -f1)

	echo -e "${Green_font_prefix}${myhash}${Font_color_suffix}"

	if [ $myhash != "${zcash_sha256sum}" ] ; then 
		echo -e "${Error} 校验失败! "  && rm -rf "zcash-${zcash_ver}-linux64.tar.gz" && exit 1
		# echo -e "${Error} 校验失败! "   && exit 1
	else
		echo -e "${Info} 校验成功!${Font_color_suffix}"
	fi
	echo -e "${Info} zcash下载完成 !"
}

# 解压
ZCASH_Unzip(){

	echo -e "${Info} zcash----开始解压---- ${Font_color_suffix}"	
	tar -xvf "zcash-${zcash_ver}-linux64.tar.gz"
	[[ ! -e "${user_home}/zcash-${zcash_ver}/" ]] && echo -e "${Error} Zcash 解压失败 !" && rm -rf "zcash-${zcash_ver}-linux64.tar.gz" && exit 1
	# [[ ! -e "${user_home}/zcash-${zcash_ver}/" ]] && echo -e "${Error} Zcash 解压失败 !"  && exit 1
	echo -e "${Info} zcash----解压完成---- ${Font_color_suffix}"	
}
# 取
ZCASH_Fetch(){
	echo -e "${Info} ----zcash-fetch-params---- ${Font_color_suffix}"
	mv -t /usr/local/bin/ ${user_home}/zcash-${zcash_ver}/bin/*
	zcash-fetch-params
}
# 配置zcash
ZCASH_Config(){
	echo -e "${Info} ----zcash---写入配置---- ${Font_color_suffix}"
	mkdir -p ${user_home}/.zcash
	echo "addnode=mainnet.z.cash" >${user_home}/.zcash/zcash.conf
	# echo "rpcuser=username" >>${user_home}/.zcash/zcash.conf
	# echo "rpcpassword=`head -c 32 /dev/urandom | base64`" >>${user_home}/.zcash/zcash.conf
	echo "daemon=1" >>${user_home}/.zcash/zcash.conf

	cat "${user_home}/.zcash/zcash.conf"
	echo -e "${Info} ------配置完成---- ${Font_color_suffix}"
}

ZCASH_Install(){
	Installation_dependency
	ZCASH_Dowmload
	ZCASH_Unzip
	ZCASH_Fetch
	ZCASH_Config
}

ZCASH_Run(){
	sudo zcashd
	SH_Init
}

ZCASH_Stop(){
	sudo zcash-cli stop
	SH_Init
}

ZCASH_Getinfo(){
	sudo zcash-cli getinfo
	SH_Init
}

# 获取新的加密地址
ZCASH_GetNewAddress(){
	sudo zcash-cli z_getnewaddress
	SH_Init
}
# 获取加密地址列表
ZCASH_ListAddress(){

# ["123","456"]

	sudo zcash-cli z_listaddresses
	SH_Init
}

# 获取发币状态
ZCASH_GetStatus(){
	sudo zcash-cli z_getoperationstatus
	SH_Init
}
# 获取钱包总金额
ZCASH_GetBalance(){
	sudo zcash-cli z_gettotalbalance
	SH_Init
}

ZCASH_GetAddressBalance(){
	sudo zcash-cli z_getbalance $1
	# echo "0.0000$1"
}

ZCASH_Validateaddress(){

	return=`sudo zcash-cli z_validateaddress $1 | jq .isvalid`
	# return=`cat /home/acc/address | jq .isvalid`
	echo ${return}
}

ZCASH_Sendmany(){
	#zcash-cli z_sendmany "t1" '[{"address": "zt" ,"amount": 5.0}]'
	echo -e "确认命令:${Red_font_prefix}zcash-cli z_sendmany $1  $2${Font_color_suffix}"
	echo && read -e -p "请输入y确认(任意建取消)：" ver
	if [ "${ver}" == "y" ]|| [ "${ver}" == "Y" ]; then
        	operationid=`zcash-cli z_sendmany $1  $2`
		echo -e "${Red_font_prefix}---operationid:${operationid}---${Font_color_suffix}"
        else
            echo -e "${Error} ---取消发送---"
        fi
	SH_Init	
}

# 
ZCASH_Send_JSON(){

	echo -e "${Green_font_prefix}--------------钱包信息--------------${Font_color_suffix}"

	# Json=(`cat /home/acc/json.sh | jq .[]`)
	Json=(`sudo zcash-cli z_listaddresses | jq .[]`)

	for i in "${!Json[@]}"; 
	do     	
		balance=`ZCASH_GetAddressBalance ${Json[$i]}`
		printf "${Green_font_prefix}%s${Font_color_suffix}\t地址:${Green_font_prefix}%s${Font_color_suffix}\t余额:${Red_font_prefix}%s${Font_color_suffix}\n" "$i" "${Json[$i]}"  "${balance}"
	done

	echo && read -e -p "请输入要使用的钱包序号：" num

	fromaddress=${Json[$num]}
	printf "发送地址:${Green_font_prefix} ${fromaddress} ${Font_color_suffix}\n"
	echo && read -e -p "请输入要接受的地址：" addresses
	printf "接受地址:${Green_font_prefix} ${addresses} ${Font_color_suffix}\n"
	result=`ZCASH_Validateaddress $addresses`
	printf "地址验证:${Green_font_prefix} ${result} ${Font_color_suffix}\n"


	echo && read -e -p "请输入要发送的数量：" amount
	printf "发送数量:${Green_font_prefix} ${amount} ${Font_color_suffix}\n"

	sendjson="[{\"address\":\"${addresses}\",\"amount\":${amount}}]"
	# echo -e "${sendjson}"

	ZCASH_Sendmany $fromaddress $sendjson

	# SH_Init
}

SH_EXIT(){
	exit 1
}



SH_Menus(){
	echo -e "**********${Green_font_prefix}Zcash 帅比管理脚本${Red_font_prefix}[V${sh_ver}]${Font_color_suffix}**********
----------------------------
	${Green_font_prefix}1.${Font_color_suffix} 安装Zcash${zcash_ver}
----------------------------
	${Green_font_prefix}2.${Font_color_suffix} 启动Zcash
	${Green_font_prefix}3.${Font_color_suffix} 停止Zcash
----------------------------
	${Green_font_prefix}4.${Font_color_suffix} Zcash-getinfo
	${Green_font_prefix}5.${Font_color_suffix} 获取钱包总金币
	${Green_font_prefix}6.${Font_color_suffix} 获取新的加密地址
	${Green_font_prefix}7.${Font_color_suffix} 获取加密地址列表
	${Green_font_prefix}8.${Font_color_suffix} 获取转币状态
	${Green_font_prefix}9.${Font_color_suffix} 转币
----------------------------
	${Green_font_prefix}00.${Font_color_suffix} 退出
	${Green_font_prefix}01.${Font_color_suffix} 显示菜单
	"
	SH_Init
}

SH_Init(){

	echo && read -e -p "请输入数字[1-8](00:退出,01:菜单)：" num
	case "$num" in
		00)
		SH_EXIT
		;;
		01)
		SH_Menus
		;;
		1)
		ZCASH_Install
		;;
		2)
		ZCASH_Run
		;;
		3)
		ZCASH_Stop
		;;
		4)
		ZCASH_Getinfo
		;;
		5)
		ZCASH_GetBalance
		;;
		6)
		ZCASH_GetNewAddress
		;;
		7)
		ZCASH_ListAddress
		;;
		8)
		ZCASH_GetStatus
		;;
		9)
		ZCASH_Send_JSON
		;;
		*)
		echo -e "${Error} 请输入正确的数字 [1-8]"
		;;
	esac
}
check_sys
SH_Menus